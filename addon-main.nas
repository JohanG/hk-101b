var main = func(addon) {

    var addonMessagePrefix = "Hand camera HK 101B add-on:  ";

    var addonPropRoot       = "/addons/by-id/" ~ addon.id;

    # Add-on version
    var addonVersion         = props.globals.getNode(addonPropRoot ~ "/version", 1);

    # Announce that the add-on is being loaded
    print(addonMessagePrefix ~ "Add-on loading (version " ~ addonVersion.getValue() ~")");

    # Camera configuration leaf nodes
    var frameLengthNode      = props.globals.getNode(addonPropRoot ~ "/config/long-side-of-frame-mm", 1);
    var framesPerRollNode    = props.globals.getNode(addonPropRoot ~ "/config/frames-per-roll", 1);
    var extraFramesNode      = props.globals.getNode(addonPropRoot ~ "/config/extra-frames-per-roll", 1);

    # Camera user settings leaf nodes (these will be saved on /sim/signals/exit)
    var enableSoundNode                      = props.globals.getNode(addonPropRoot ~ "/settings/enable-shutter-sound", 1);
    var enableFovChangeNode                  = props.globals.getNode(addonPropRoot ~ "/settings/enable-field-of-view-change", 1);
    var enableHidingMenuBarNode              = props.globals.getNode(addonPropRoot ~ "/settings/enable-hiding-menu-bar", 1);
    var enableHidingFrameLatencyDisplayNode  = props.globals.getNode(addonPropRoot ~ "/settings/enable-hiding-frame-latency-display", 1);
    var enableHidingFpsDisplayNode           = props.globals.getNode(addonPropRoot ~ "/settings/enable-hiding-fps-display", 1);
    var enableAutoReloadNode                 = props.globals.getNode(addonPropRoot ~ "/settings/enable-auto-reload", 1);
    var focalLengthNode                      = props.globals.getNode(addonPropRoot ~ "/settings/lens-focal-length-mm", 1);

    # Camera status leaf nodes
    var reloadFilmNode        = props.globals.getNode(addonPropRoot ~ "/status/reload-film", 1);
    var frameCounterNode      = props.globals.getNode(addonPropRoot ~ "/status/frame-counter", 1);
    var noFramesLeftNode      = props.globals.getNode(addonPropRoot ~ "/status/no-frames-left", 1);
    var shutterPressedNode    = props.globals.getNode(addonPropRoot ~ "/status/shutter-pressed", 1);

    # Other leaf nodes
    var enableInstantSoundQueue     = props.globals.getNode("/sim/sound/instant/enabled", 1);
    var currentFovNode              = props.globals.getNode("/sim/current-view/field-of-view", 1);
    var simFrameCountNode           = props.globals.getNode("/sim/rendering/sim-frame-count", 1);
    var exitSignalNode              = props.globals.getNode("/sim/signals/exit", 1);
    var menubarVisibilityNode        = props.globals.getNode("/sim/menubar/visibility", 1);
    var frameLatencyDisplayNode     = props.globals.getNode("/sim/rendering/frame-latency-display", 1);
    var fpsDisplayNode              = props.globals.getNode("/sim/rendering/fps-display", 1);

    # Defining a settings file and reading and saving it
    var settingsFile = nil;
    settingsFile = addon.createStorageDir() ~ "/settings.xml";
    var settingsNode = props.globals.getNode(addonPropRoot ~ "/settings");

    var readSettings = func() {
        io.read_properties(settingsFile, settingsNode);
    };

    # writeSettings is called from exitSignalListener
    var writeSettings = func() {
        io.write_properties(settingsFile, settingsNode);
    };

    readSettings();

    # Defining some variables for future use, all nil for now
    var preCaptureFov = nil;
    var preCaptureMenubarVisibility = nil;
    var preCaptureFrameLatencyDisplay = nil;
    var preCaptureFpsDisplay = nil;
    var lensFov = nil;
    var goForExposure = {fov: 0, menuBar: 0, frameLatency: 0, fps: 0};
    var exposureFrame = nil;

    var shutterSoundPlaying = 0;

    var shutterSound = props.Node.new({
        queue: "instant",
        path: addon.basePath ~ "/Sounds",
        file: "Film_SLR_single_shot.wav",
        volume: 1.0
    });

    var calcLensFov = func() {
        return 2 * math.atan2(frameLengthNode.getValue()/2, focalLengthNode.getValue()) * 180/math.pi;
    };

    var playShutterSound = func() {
        if ((shutterSoundPlaying == 0) and (enableSoundNode.getValue() == 1)) {
            shutterSoundPlaying = 1;
            enableInstantSoundQueue.setValue(1);
            fgcommand("play-audio-sample", shutterSound);
            shutterSoundPlaying = 0;
        };
        
    };

    var areThereFramesLeft = func() {
        if (frameCounterNode.getValue() >= (framesPerRollNode.getValue() + extraFramesNode.getValue())) {
            if (noFramesLeftNode.getValue() == 0) {
                noFramesLeftNode.setBoolValue(1);
                print(addonMessagePrefix ~ "All film exposed");
            };
        };
    };

    var whenShutterPressed = func() {
        if (shutterPressedNode.getValue() == 1) {
            if (noFramesLeftNode.getValue() == 0) {
                triggerExposure();
            } else {
                if (enableAutoReloadNode.getValue() == 1) {
                    reloadFilmNode.setBoolValue(1);
                    triggerExposure();
                };
            };
            shutterPressedNode.setBoolValue(0);
        };
    };

    var triggerExposure = func() {
        # Save current values of properties
        preCaptureFov = view.fovProp.getValue();
        preCaptureMenubarVisibility = menubarVisibilityNode.getValue();
        preCaptureFrameLatencyDisplay = frameLatencyDisplayNode.getValue();
        preCaptureFpsDisplay = fpsDisplayNode.getValue();
        # Set up all properties before taking the screenshot
        if (enableFovChangeNode.getValue() == 1) {
            lensFov = calcLensFov();
            view.fovProp.setDoubleValue(lensFov);
        };
        if (enableHidingMenuBarNode.getValue() == 1) {
            menubarVisibilityNode.setBoolValue(0);
        };
        if (enableHidingFrameLatencyDisplayNode.getValue() == 1) {
            frameLatencyDisplayNode.setBoolValue(0);
        };
        if (enableHidingFpsDisplayNode.getValue() == 1) {
            fpsDisplayNode.setBoolValue(0);
        };
        readyForExposureYetTimer.start();
    };

    var readyForExposureYet = func {
        # Checks if all properties have reached desired values once per
        # rendering frame, and when they have reached desired values
        # call exposeFrame() to take the screenshot.
        #
        # Currently something fails in this function on the last line
        # or a bit up if last line is commented out.
        if (enableFovChangeNode.getValue() == 1) {
            if (currentFovNode.getValue() == lensFov) {
                goForExposure.fov = 1;
            } else {
                goForExposure.fov = 0;
            };
        } else {
            goForExposure.fov = 1;
        };
        if (enableHidingMenuBarNode.getValue() == 1) {
            if (menubarVisibilityNode.getValue() == 0) {
                goForExposure.menuBar = 1;
            } else {
                goForExposure.menuBar = 0;
            };
        } else {
            goForExposure.menuBar = 1;
        };
        if (enableHidingFrameLatencyDisplayNode.getValue() == 1) {
            if (frameLatencyDisplayNode.getValue() == 0) {
                goForExposure.frameLatency = 1;
            } else {
                goForExposure.frameLatency = 0;
            };
        } else {
            goForExposure.frameLatency = 1;
        };
        if (enableHidingFpsDisplayNode.getValue() == 1) {
            if (fpsDisplayNode.getValue() == 0) {
                goForExposure.fps = 1;
            } else {
                goForExposure.fps = 0;
            };
        } else {
            goForExposure.fps = 1;
        };
        # If all properties are at desired values,
        # take the screenshot.
        if ((goForExposure.fov == 1) and (goForExposure.menuBar == 1) and (goForExposure.frameLatency == 1) and (goForExposure.fps == 1)) {
            exposeFrame();
        };
    };

    var exposeFrame = func() {
        # Takes the screenshot.
        playShutterSound();
        fgcommand("screen-capture");
        exposureFrame = simFrameCountNode.getValue();
        readyForExposureYetTimer.stop();
        restoreSettingsAfterExposureTimer.start();
        frameCounterNode.setIntValue(frameCounterNode.getValue() + 1);
        print(addonMessagePrefix ~ "Shutter pressed (frame ", frameCounterNode.getValue(), " / " ~ framesPerRollNode.getValue() ~ ")");
    };

    var restoreSettingsAfterExposure = func() {
        # If I am not mistaken, this waits one rendering frame after
        # the screenshot is taken, then restores properties to
        # original values.
        if (simFrameCountNode.getValue() > (exposureFrame)) {
            view.fovProp.setDoubleValue(preCaptureFov);
            menubarVisibilityNode.setBoolValue(preCaptureMenubarVisibility);
            frameLatencyDisplayNode.setBoolValue(preCaptureFrameLatencyDisplay);
            fpsDisplayNode.setBoolValue(preCaptureFpsDisplay);
            restoreSettingsAfterExposureTimer.stop();
        };
    };

    var reloadFilm = func() {
        if (reloadFilmNode.getValue() == 1) {
            frameCounterNode.setIntValue(0);
            noFramesLeftNode.setBoolValue(0);
            reloadFilmNode.setBoolValue(0);
            if  (enableAutoReloadNode.getValue() == 1) {
                print(addonMessagePrefix ~ "Film automatically reloaded, you cheater ;-)");
            } else {
                print(addonMessagePrefix ~ "Film reloaded");
            };
        };
    };

    # Print camera settings if triggered by listeners (on startup and on change)
    var printSoundSetting = func() {
        if (enableSoundNode.getValue() == 1) {
            print(addonMessagePrefix ~ "Shutter sound enabled");
        }
        else if (enableSoundNode.getValue() == 0) {
            print(addonMessagePrefix ~ "Shutter sound disabled");
        };
    };

    var printFovChangeSetting = func() {
        if (enableFovChangeNode.getValue() == 1) {
            print(addonMessagePrefix ~ "Field of view change enabled");
        }
        else if (enableFovChangeNode.getValue() == 0) {
            print(addonMessagePrefix ~ "Field of view change disabled");
        };
    };

    var printHidingMenuBarSetting = func() {
        if (enableHidingMenuBarNode.getValue() == 1) {
            print(addonMessagePrefix ~ "Hiding menu bar enabled");
        }
        else if (enableHidingMenuBarNode.getValue() == 0) {
            print(addonMessagePrefix ~ "Hiding menu bar disabled");
        };
    };

    var printHidingFrameLatencyDisplaySetting = func() {
        if (enableHidingFrameLatencyDisplayNode.getValue() == 1) {
            print(addonMessagePrefix ~ "Hiding frame latency display enabled");
        }
        else if (enableHidingFrameLatencyDisplayNode.getValue() == 0) {
            print(addonMessagePrefix ~ "Hiding frame latency display disabled");
        };
    };

    var printHidingFpsDisplaySetting = func() {
        if (enableHidingFpsDisplayNode.getValue() == 1) {
            print(addonMessagePrefix ~ "Hiding FPS display enabled");
        }
        else if (enableHidingFpsDisplayNode.getValue() == 0) {
            print(addonMessagePrefix ~ "Hiding FPS display disabled");
        };
    };

    var printAutoReloadSetting = func() {
        if (enableAutoReloadNode.getValue() == 1) {
            print(addonMessagePrefix ~ "Automatic film reloading enabled, you cheater ;-)");
        }
        else if (enableAutoReloadNode.getValue() == 0) {
            print(addonMessagePrefix ~ "Automatic film reloading disabled");
        };
    };

    var printFocalLengthSetting = func() {
        # No need to show this if it does not affect the screenshots
        if (enableFovChangeNode.getValue() == 1) {
            print(addonMessagePrefix ~ "Lens focal length " ~ focalLengthNode.getValue() ~ " mm");
        };
    };

    # Timers
    var readyForExposureYetTimer = maketimer(0, readyForExposureYet);
    var restoreSettingsAfterExposureTimer = maketimer(0, restoreSettingsAfterExposure);

    # Camera settings listeners (triggered on startup and when changed)
    var enableSoundListener                      = setlistener(enableSoundNode, printSoundSetting, 1, 0);
    var enableFovChangeListener                  = setlistener(enableFovChangeNode, printFovChangeSetting, 1, 0);
    var enableHidingMenuBarListener              = setlistener(enableHidingMenuBarNode, printHidingMenuBarSetting, 1, 0);
    var enableHidingFrameLatencyDisplayListener  = setlistener(enableHidingFrameLatencyDisplayNode, printHidingFrameLatencyDisplaySetting, 1, 0);
    var enableHidingFpsDisplayListener           = setlistener(enableHidingFpsDisplayNode, printHidingFpsDisplaySetting, 1, 0);
    var enableAutoReloadListener                 = setlistener(enableAutoReloadNode, printAutoReloadSetting, 1, 0);
    var focalLengthListener                      = setlistener(focalLengthNode, printFocalLengthSetting, 1, 0);

    # Other listeners
    var frameCounterListener          = setlistener(frameCounterNode, areThereFramesLeft, 1, 0);
    var reloadFilmListener            = setlistener(reloadFilmNode, reloadFilm, 0, 0);
    var shutterPressedListener        = setlistener(shutterPressedNode, whenShutterPressed, 0, 0);
    var exitSignalListener            = setlistener(exitSignalNode, writeSettings, 0, 0);

    # Announce that the add-on is loaded
    print(addonMessagePrefix ~ "Add-on loaded");
}

var unload = func(addon) {
    print(addonMessagePrefix ~ "Add-on unloaded");
    writeSettings();
};
